#!/usr/bin/env python2.7
"""
PHYS3061 Project: StarDisplay
Filename: Analysis.py
# Descriptions
- Perform analysis on StarDisplay simulation results
# Required packages
- numpy
- ffmpeg
# Author
- Written by Ka-Lok, LO (SID: 1155063995)
"""

import ReadXYZ
import numpy as np
import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot as plt
import matplotlib.animation as animation
from optparse import OptionParser

parser = OptionParser(description="Performing analysis on the result from StarDisplay")
parser.add_option("--inputXYZFilename", type = "string", help = "The path of the xyz file to be analysed. Required")
parser.add_option("-v", "--verbose", action="store_true", default = False, help = "Be verbose")
parser.add_option("--video", action = "store_true", default = False, help = "Output video")
parser.add_option("--timeStep", type = "float", help = "The time step, delta t, for each time evolution in the unit s. Required.")
parser.add_option("--image", action = "store_true", default = False, help = "Output still images")
parser.add_option("--RDF", action = "store_true", default = False, help = "Computing RDF")
parser.add_option("--frameNo", type = "int", help = "The frame no that the RDF will be computed.")
parser.add_option("--maxR", type = "float", help = "The maximum radial distance from any one particle to another in the unit of m. Required.")
parser.add_option("--dr", type = "float", help = "The dr in finding the radial distribution function in the unit of m. When this option is given, the program will compute the radial distribution function at the end of the evolution of the system. Required.")
(options, args) = parser.parse_args()

# Checking arguments
requiredOptions = ["inputXYZFilename"]
missingOptions = [option for option in requiredOptions if getattr(options,option) is None]
if(missingOptions != []):
    print "The following option(s) is/are missing:", ", ".join(missingOptions)
assert missingOptions == []

# Reading the xyz file
xyzFile = ReadXYZ.XYZ(options.inputXYZFilename)
frameData = xyzFile.parse()

def computeNormalizedNavieRDF(R, frameData):
    # This implements the navie algorithm to find the normalized RDF
    count = 0
    calculateRadialDistance = lambda x, y, z : (x**2 + y**2 + z**2)**(0.5) # Calculate the radial distance
    totalNumber = len(frameData)
    for i in range(totalNumber):
        for j in range(totalNumber):
            if i == j:
                pass
            else:
                xDist = frameData[i][0] - frameData[j][0]
                yDist = frameData[i][1] - frameData[j][1]
                zDist = frameData[i][2] - frameData[j][2]
                radialDist = calculateRadialDistance(xDist, yDist, zDist)
                if(R <= radialDist and radialDist < R + options.dr):
                    count += 1

    def normalizeNavieRDF(R, count):
        # g(R) = R/(3*dr*N^2)
        return R/(3.0*options.dr*totalNumber**2)*count

    return normalizeNavieRDF(R, count)

# Compute the radial distribution function
if(options.RDF):
    assert options.maxR != float("+inf"), "I cannot compute the radial distribution function indefinitely!"
    rList = np.arange(options.dr, options.maxR, options.dr)
    countList = np.zeros(len(rList))
    lastFrameData = frameData[options.frameNo]
    for index, R in enumerate(rList):
        countList[index] = computeNormalizedNavieRDF(R, lastFrameData.values())

    # Plotting the RDF
    plt.figure()
    plt.title("Normalized Radial Distribution Function $g(r)$")
    plt.xlabel(r"$r$ (m)")
    plt.ylabel(r"Probability")
    plt.scatter(rList, countList, marker='^', color = 'b')
    plt.plot(rList, countList)
    plt.grid()
    plt.savefig("RDF.png")


if(options.image):
    for frameNo, frameContent in enumerate(frameData):
        fig = plt.figure()
        if(options.verbose):
            print "Generating frame %i" % frameNo
        for starling in frameContent.values():
            plt.scatter(starling[0], starling[1], s = 50)
        plt.xlabel(r"$x$ (m)")
        plt.ylabel(r"$y$ (m)")
        plt.savefig("Frame%i.png" % frameNo)
        plt.close(fig)

def update_plot(i, data, ax):
    plt.cla()
    coordinateXList = []
    coordinateYList = []
    if(options.verbose):
        print "Generating frame %i" % i
    for starling in data[i].values():
        coordinateXList.append(starling[0])
        coordinateYList.append(starling[1])
    ax.set_xlabel(r"$x$ (m)")
    ax.set_ylabel(r"$y$ (m)")
    ax.set_title(r"Time $t = %.2f$ s" % (options.timeStep * i))
    ax.scatter(coordinateXList, coordinateYList)
    return ax,

if(options.video):
    Writer = animation.writers['ffmpeg']
    writer = Writer(fps=30, metadata=dict(title='Simulation result of StarDisplay', artist='Matplotlib', comment='PHYS3061 Project'), bitrate=1800)
    fig = plt.figure()
    ax = plt.gca()
    ax.autoscale()
    ani = animation.FuncAnimation(fig, update_plot, frames=xrange(len(frameData)), fargs=(frameData, ax))
    ani.save('StarlingVideo.mp4')