"""
PHYS3061 Lab 2: Particle Initialization, Trajectory, and Boundary Conditions
Filename: GenerateXYZ.py
# Descriptions
- Generating the XYZ file for visualization purpose
# Author
- Written by Ka-Lok, LO (SID: 1155063995)
"""

class XYZ:
    def __init__(self, filename, items):
        self.filename = filename
        assert not (self.filename is None), "Filename is required."
        self.items = items
        assert not (self.items is None), "No item is given!"
        self.fileHandler = open(filename,"w")
        self.noOfFrames = 0
    
    def writeFrame(self, positions):
        assert len(self.items) == len(positions), "Not enough positions are given!"
        self.noOfFrames += 1
        self.fileHandler.write("%d\n" % (len(self.items)))
        self.fileHandler.write("Frame %d\n" % (self.noOfFrames))
        for i in range(len(self.items)):
            self.fileHandler.write("%s %.3f %.3f %.3f\n" % (self.items[i], positions[i][0], positions[i][1], positions[i][2]))

    def closeFile(self):
        self.fileHandler.close()
        
