nx = 10
ny = 5
nz = 1
Lx = 2
Ly = 4
Lz = 1
timeStep = 0.01
noOfTimeStepPerFrame = 1
totalNoOfTimeStep = 1000
filename = starlings2.xyz

all: image video RDF

$(filename):
	./StarDisplay.py --filename=$(filename) --nx=$(nx) --ny=$(ny) --nz=$(nz) --Lx=$(Lx) --Ly=$(Ly) --Lz=$(Lz) --timeStep=$(timeStep) --noOfTimeStepPerFrame=$(noOfTimeStepPerFrame) --totalNoOfTimeStep=$(totalNoOfTimeStep)

video:
	./Analysis.py --inputXYZFilename=$(filename) --video --timeStep=$(timeStep)

image:
	./Analysis.py --inputXYZFilename=$(filename) --image
	mkdir -p StillImages
	mv Frame*.png StillImages/

RDF:
	./Analysis.py --inputXYZFilename=$(filename) --RDF --maxR=15 --dr=0.2 --frameNo=654

clean:
	rm *.png *.mp4