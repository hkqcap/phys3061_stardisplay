"""
PHYS3061 Lab 4: Computing Statistics and Thermodynamic Equilibirum
Filename: ReadXYZ.py
# Descriptions
- Parsing XYZ file
# Author
- Written by Ka-Lok, LO (SID: 1155063995)
"""

class XYZ:
    def __init__(self, filename):
        self.fileHandler = open(filename, "r") # Importing the file

    def parse(self):
        noOfItems = 0
        rawData = [] # Storing the raw data

        # Parsing the xyz file
        tempData = {}
        for lineNo, lineContent in enumerate(self.fileHandler):
            if(lineNo == 0):
                # This line should contain the total number of walkers in the xyz file
                noOfItems = int(lineContent)
                continue
            if(lineNo % (noOfItems + 2) == 0):
                # This line should be the first line for each frame. Discard this line
                rawData.append(tempData)
                tempData = {}       
            elif(lineNo % (noOfItems + 2) == 1):
                # This line should be the comment line. Discard this line
                continue
            else:
                # This line should contain name of the walker and the actual coordinates
                raw = lineContent.split(" ")
                tempData[raw[0]] = (float(raw[1]), float(raw[2]), float(raw[3]))

        if(tempData != {}):
            rawData.append(tempData)
            tempData = {}
        return rawData

    def closeFile(self):
        self.fileHandler.close()
