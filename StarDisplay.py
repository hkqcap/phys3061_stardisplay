#!/usr/bin/env python2.7
"""
PHYS3061 Project: StarDisplay
Filename: StarDisplay.py
# Descriptions
- Implement StarDisplay model
- Roosting is not implemented
- Banking angle of starlings are not calculated
- Verlet intergator is used instead of Euler's method
# Required packages
- numpy
- scipy
# Author
- Written by Ka-Lok, LO (SID: 1155063995)
"""

import copy
import numpy as np
import scipy
import GenerateXYZ
from optparse import OptionParser

parser = OptionParser(description="Simulating the flocking of starlings by self-organization from rules of co-ordination with nearby neighbors.")
parser.add_option("--filename", type = "string", help = "The path for outputing the xyz file of the system for later visualization by VMD. Required.")
parser.add_option("--nx", type = "int", help = "The number of starlings on the lattice in x-direction initially. Required.")
parser.add_option("--ny", type = "int", help = "The number of starlings on the lattice in y-dircetion initially. Required.")
parser.add_option("--nz", type = "int", help = "The number of starlings on the lattice in z-direction initially. Required.")
parser.add_option("--Lx", type = "float", help = "Length of the lattice in x-direction in the unit of m. Required.")
parser.add_option("--Ly", type = "float", help = "Length of the lattice in y-direction in the unit of m. Required.")
parser.add_option("--Lz", type = "float", help = "Length of the lattice in z-direction in the unit of m. Required.")
parser.add_option("--xBox", type = "float", default=float("+inf"), help = "Length of the box in x-direction in the unit m.")
parser.add_option("--yBox", type = "float", default=float("+inf"), help = "Length of the box in y-direction in the unit m.")
parser.add_option("--zBox", type = "float", default=float("+inf"), help = "Length of the box in z-direction in the unit m.")
parser.add_option("--timeStep", type = "float", help = "The time step, delta t, for each time evolution in the unit s. Required.")
parser.add_option("--noOfTimeStepPerFrame", type = "int", help = "Number of time steps per output frame. Required.")
parser.add_option("--totalNoOfTimeStep", type = "int", help = "Number of time steps performed in each simulation. Required.")
parser.add_option("-v", "--verbose", action="store_true", default = False, help = "Be verbose")
(options, args) = parser.parse_args()

# Checking arguments
requiredOptions = ["filename", "nx", "ny", "nz", "Lx", "Ly", "Lz", "timeStep", "noOfTimeStepPerFrame", "totalNoOfTimeStep"]
missingOptions = [option for option in requiredOptions if getattr(options,option) is None]
if(missingOptions != []):
    print "The following option(s) is/are missing:", ", ".join(missingOptions)
assert missingOptions == []

class ManyParticleSystem:
    def __init__(self):
        self.states = {}
        self.totalNumber = options.nx * options.ny * options.nz
        particlePositions = np.zeros((self.totalNumber,3), dtype = np.float)
        particleVelocities = np.zeros((self.totalNumber,3), dtype = np.float)
        particleForces = np.empty((self.totalNumber,3), dtype = np.float)
        particleForces = particleForces.fill(np.nan) # Fill the particle forces with NaN
        self.states["t"] = {"Positions": particlePositions, "Velocities": particleVelocities, "Forces": particleForces}
        self.parameters = {
        "mass": 0.08,
        "cruiseSpeed" : 10.0,
        "relaxationTime": 1.0,
        "rMaxSeparation": 0.2,
        "rSep": 4.0,
        "reactionTime": 10*options.timeStep,
        "rMax": 100.0,
        "interpolationFactor": 0.1*10*options.timeStep,
        "nc": 6.5,
        "weightingFactorSeparation": 1.0,
        "weightingFactorAlignment": 0.5,
        "weightingFactorCohesion": 1.0,
        "weightingFactorRandom": 0.01,
        "sigma": 1.37,
        "thrust": 0.24,
        "liftDragCoefficient": 3.3,
        "rearBlindAngle": 90} # A dictionary of parameters
        self.particleInteractionRanges = [np.zeros((self.totalNumber,), dtype = np.float) for i in range(int(self.parameters["reactionTime"]/options.timeStep))]

    """
    Particle Initialization
    """

    def initializePositions(self):
        # Initializing the position of particles on a lattice
        dx = options.Lx/options.nx
        dy = options.Ly/options.ny
        dz = options.Lz/options.nz
        # Assigning the position of particles in row-major order: x=0, y=0, z=0,....,nz-1; x=0, y=1, z=0,....,n_z-1, etc.
        for i in range(options.nx):
            for j in range(options.ny):
                for k in range(options.nz):
                    index = i*options.ny*options.nz + j*options.nz + k
                    self.states["t"]["Positions"][index] = [dx*i, dy*j, dz*k]
        
    def initializeVelocities(self):
        for index in range(len(self.states["t"]["Velocities"])):
            self.states["t"]["Velocities"][index][0] = np.random.normal(loc = self.parameters["cruiseSpeed"], scale = 1.0)
            self.states["t"]["Velocities"][index][1] = np.random.normal(loc = 0.0, scale = 0.1)
            self.states["t"]["Velocities"][index][2] = 0.0 # zero velocity in z-direction

    def initializeInteractionRanges(self):
        for i in range(len(self.particleInteractionRanges)):
                self.particleInteractionRanges[i] = [np.random.uniform(self.parameters["rMaxSeparation"], self.parameters["rSep"]) for k in range(self.totalNumber)]

    """
    Force Calculation
    """

    def calculateTotalForce(self, time):
        """
        Sketch of the algorithm:
        - for each starling i, calculate the force acting on starling i by starling j (where i =/= j)
        - force = f_{flight} + f_{steering}
        - f_{flight} is the aerodynamic drag and lift force
        - f_{flight} = lift + drag + thrust + weight
        - f_{steering} = f_{social} + f_{speed} + f_{roost} (not implemented) + f_{random}
        - f_{social} = f_{s} + f_{a} + f_{c}
        - f_{s}: force of separation
        - f_{a}: force of alignment
        - f_{c}: force of cohesion
        """
        # The force of speed
        def calculateForceSpeed(mass, relaxationTime, cruiseSpeed, speed):
            # Force of speed = mass/relaxationTime x (cruise speed - current speed) e_{x} where x is the forward direction
            return np.array([(mass/float(relaxationTime)) * (cruiseSpeed - speed), 0, 0], dtype = np.float)

        def calculateForceSpeedUpdated(mass, relaxationTime, cruiseSpeedX, cruiseSpeedY, cruiseSpeedZ, speedX, speedY, speedZ):
            return (mass/float(relaxationTime))*np.array([(cruiseSpeedX - speedX), (cruiseSpeedY - speedY), (cruiseSpeedZ - speedZ)], dtype = np.float)

        # The force of random
        def calculateForceRandom(weightingFactorRandom):
            np.random.seed()
            randomX = np.random.uniform(-1.0, 1.0)
            randomY = np.random.uniform(-1.0, 1.0)
            randomZ = np.random.uniform(-1.0, 1.0)
            calculateRadialDistance = lambda x, y, z : (x**2 + y**2 + z**2)**(0.5)
            normalizationConstant = float(calculateRadialDistance(randomX, randomY, randomZ))
            return weightingFactorRandom*(np.array([randomX, randomY, randomZ], dtype = np.float)/normalizationConstant)

        # The force of flight
        def calculateForceFlight(cruiseSpeed, currentSpeed, mass, liftDragCoefficient, thrust):
            lift = ((currentSpeed/float(cruiseSpeed))**2)*mass*9.81
            drag = (1.0/(liftDragCoefficient))*lift
            #return np.array([0, 0, 0], dtype = np.float)
            return np.array([-1*drag + thrust, 0, lift - mass*9.81], dtype = np.float)

        # Useful lambda expression to calculate the radial distance
        calculateRadialDistance = lambda x, y, z : (x**2 + y**2 + z**2)**(0.5)

        totalForceOnParticles = np.ndarray(shape = (len(self.states[time]["Positions"]), 3), dtype = np.float)
        totalForceOnParticles.fill(0.0)

        currentRange = self.particleInteractionRanges.pop(0)
        rangeForLaterTime = np.zeros((len(self.states[time]["Positions"]),), dtype = np.float)
        """
        Start looping over the particles
        """
        for i in range(len(self.states[time]["Positions"])):
            # Calculate the non-interaction forces
            if(options.verbose):
                print "Starling %d: current velocity" % i
                print self.states[time]["Velocities"][i]
            currentSpeed = np.linalg.norm(self.states[time]["Velocities"][i])
            forceOfSpeed = calculateForceSpeed(self.parameters["mass"], self.parameters["relaxationTime"], self.parameters["cruiseSpeed"], currentSpeed)
            #forceOfSpeed = calculateForceSpeedUpdated(self.parameters["mass"], self.parameters["relaxationTime"], self.parameters["cruiseSpeed"], 0.0, 0.0, self.states[time]["Velocities"][i][0], self.states[time]["Velocities"][i][1], self.states[time]["Velocities"][i][2])
            forceOfRandom = calculateForceRandom(self.parameters["weightingFactorRandom"])
            forceOfFlight = calculateForceFlight(self.parameters["cruiseSpeed"], currentSpeed, self.parameters["mass"], self.parameters["liftDragCoefficient"], self.parameters["thrust"])
            totalForceOnParticles[i] = forceOfFlight + forceOfRandom + forceOfSpeed

            if(options.verbose):
                print "Force of speed", forceOfSpeed
                print "Force of random", forceOfRandom
                print "Force of flight", forceOfFlight

            # Calculate the interaction force/social force
            forceOfSeparationOnParticle = np.array([0.0, 0.0, 0.0], dtype = np.float)
            forceOfAlignmentOnParticle = np.array([0.0, 0.0, 0.0], dtype = np.float)
            forceOfAlignmentConstant = 0.0
            forceOfCohesionOnParticle = np.array([0.0, 0.0, 0.0], dtype = np.float)
            neighborhoodOfIndividual = 0.0
            reducedNeighborhood = 0.0
            neighborhoodG = 0.0
            centrality = 0.0
    
            for j in range(len(self.states[time]["Positions"])):
                if(i == j):
                    # There is no need to compute the interaction force since the particle won't intereact with itself
                    continue # nothing to calculate
                else:
                    xDist = self.states[time]["Positions"][j][0] - self.states[time]["Positions"][i][0]
                    yDist = self.states[time]["Positions"][j][1] - self.states[time]["Positions"][i][1]
                    zDist = self.states[time]["Positions"][j][2] - self.states[time]["Positions"][i][2]
                    radialDist = float(calculateRadialDistance(xDist, yDist, zDist))
                    unitVector = np.array([xDist, yDist, zDist], dtype = np.float)/radialDist

                    # Counting the neighborhood of individual
                    if(radialDist <= currentRange[i]):
                        neighborhoodOfIndividual += 1
                        # Force of separation
                        if(radialDist <= self.parameters["rMaxSeparation"]):
                            g = 1
                        else:
                            g = np.exp(-1*((radialDist - self.parameters["rMaxSeparation"])**2)/float(self.parameters["sigma"]**2))
                        forceOfSeparationOnParticle += g*unitVector
                    
                    # Counting the neighborhood G
                    if(radialDist <= 2*currentRange[i]):
                        neighborhoodG += 1
                        centrality += radialDist

                    # Counting the reduced neighborhood
                    isInReducedNeighborhood = False
                    if(radialDist <= currentRange[i]):
                        # in the neighborhood of individual
                        if(self.states[time]["Positions"][j][0] >= self.states[time]["Positions"][i][0]):
                            # starling j is ahead of starling i in the x-direction
                            reducedNeighborhood += 1
                            isInReducedNeighborhood = True
                        else:
                            # check whether starling j is within the blind zone of starling i
                            yUpper = np.tan(np.radians(180 - self.parameters["rearBlindAngle"]/2.0))*(xDist) + self.states[time]["Positions"][i][1]
                            yLower = np.tan(np.radians(180 + self.parameters["rearBlindAngle"]/2.0))*(xDist) + self.states[time]["Positions"][i][1]
                            if not (yLower <= self.states[time]["Positions"][j][1] <= yUpper):
                                reducedNeighborhood += 1
                                isInReducedNeighborhood = True
                        if(isInReducedNeighborhood):
                            # Force of alignment
                            forceOfAlignmentOnParticle += [xDist, 0, 0]
                            forceOfAlignmentConstant += abs(xDist)

                            # Force of cohesion
                            if(radialDist <= self.parameters["rMaxSeparation"]):
                                x = 0
                            else:
                                x = 1
                            forceOfCohesionOnParticle += x*unitVector

            # Finished looping over all particles
            # Calculate the range for later time
            rangeForLaterTime[i] = (1 - self.parameters["interpolationFactor"])*currentRange[i] + self.parameters["interpolationFactor"]*(self.parameters["rMax"] - self.parameters["rMax"]*(neighborhoodOfIndividual/float(self.parameters["nc"])))
            if neighborhoodG != 0.0:
                centrality = centrality/float(neighborhoodG)
            if neighborhoodOfIndividual != 0.0:
                forceOfSeparationOnParticle = (-1*self.parameters["weightingFactorSeparation"]/float(neighborhoodOfIndividual))*forceOfSeparationOnParticle
            if reducedNeighborhood != 0.0:
                forceOfCohesionOnParticle = (centrality/float(reducedNeighborhood))*self.parameters["weightingFactorCohesion"]*forceOfCohesionOnParticle
            if forceOfAlignmentConstant != 0.0:
                forceOfAlignmentOnParticle = (self.parameters["weightingFactorAlignment"]/float(forceOfAlignmentConstant))*(forceOfAlignmentOnParticle)
            
            if(options.verbose):
                print "Force of separation", forceOfSeparationOnParticle
                print "Force of cohesion", forceOfCohesionOnParticle
                print "Force of alignment", forceOfAlignmentOnParticle
            
            totalForceOnParticles[i] += (forceOfSeparationOnParticle + forceOfCohesionOnParticle + forceOfAlignmentOnParticle) # Adding the social force to the total forces on particle i
            totalForceOnParticles[i] = np.array([totalForceOnParticles[i][0], totalForceOnParticles[i][1], 0], dtype = np.float)

        # Assigning the force
        self.states[time]["Forces"] = totalForceOnParticles
        # Calculating the interaction range for later time!
        self.particleInteractionRanges.append(rangeForLaterTime)

    """
    System evolution (using Verlet Integrator)
    """

    def evolute(self):
        # Perform time evolution using verlet algorithm
        self.states["t-1"] = copy.deepcopy(self.states["t"]) # Saving the current states of the system as the previous states; This is necessary though it is VERY SLOW and RESOURCE CONSUMING
        self.states["t"]["Positions"] = self.states["t-1"]["Positions"] + self.states["t-1"]["Velocities"]*options.timeStep + (self.states["t-1"]["Forces"])*(float((options.timeStep)**2)/(2.0*self.parameters["mass"])) # Verlet Algorithm
        for index in range(len(self.states["t"]["Positions"])):
            for dimension in range(3):
                # Checking boundary conditions: hard wall
                boxLength = 0
                if(dimension == 0):
                    boxLength = options.xBox
                elif(dimension == 1):
                    boxLength = options.yBox
                elif(dimension == 2):
                    boxLength = options.zBox
                if(self.states["t"]["Positions"][index][dimension] >= boxLength):
                    self.states["t-1"]["Velocities"][index][dimension] = -1*self.states["t-1"]["Velocities"][index][dimension]
                    # Pulling the particle back
                    self.states["t"]["Positions"][index][dimension] = self.states["t-1"]["Positions"][index][dimension]
                elif(self.states["t"]["Positions"][index][dimension] <= -1*boxLength):
                    self.states["t-1"]["Velocities"][index][dimension] = -1*self.states["t-1"]["Velocities"][index][dimension]
                    # Pulling the particle back
                    self.states["t"]["Positions"][index][dimension] = self.states["t-1"]["Positions"][index][dimension]
        # Update Velocities
        self.calculateTotalForce("t") # Calculate the force
        self.states["t"]["Velocities"] = self.states["t-1"]["Velocities"] + (options.timeStep/(float(2.0*self.parameters["mass"])))*(self.states["t"]["Forces"] + self.states["t-1"]["Forces"]) # Verlet Algorithm
        # Sanity checking
        if(options.verbose):
            print self.states["t"]["Forces"]/self.states["t-1"]["Forces"]
        del self.states["t-1"] # We don't need this anymore :)

    """
    Utilities functions
    """

    def getPositions(self, time):
        return self.states[time]["Positions"]

    def getVelocities(self, time):
        return self.states[time]["Velocities"]

    def getAccelerations(self, time):
        return self.states[time]["Accelerations"]

    def generateStandardNormalDistribution(self):
        return self.RNG.random(0,1)
        """
        U1 = self.RNG1.random()
        U2 = self.RNG2.random()
        return ((-2*math.log(U1))**(0.5))*math.cos(2*math.pi*U2) # Using Box-Muller method to generate a standard normal distribution; could have use numpy.random.normal()
        """

"""
Main
"""
# Creating a system of particles
if(options.verbose):
    np.seterr(over='raise')
system = ManyParticleSystem()
xyzFile = GenerateXYZ.XYZ(options.filename, ["Starling_%d" % i for i in range(system.totalNumber)])
# Initialize the system
system.initializePositions()
system.initializeVelocities()
system.initializeInteractionRanges()
system.calculateTotalForce("t")

for i in range(options.totalNoOfTimeStep):
    if(options.verbose):
        print "Current time step: %d" % i
    if(i % options.noOfTimeStepPerFrame == 0):
        xyzFile.writeFrame(system.getPositions("t"))
    system.evolute()
xyzFile.closeFile()